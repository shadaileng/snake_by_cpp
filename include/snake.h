#ifndef _SNAKE_HEAD_
#define _SNAKE_HEAD_
#include<iostream>
using namespace std;
#include "wall.h"
#include "food.h"

class Snake {
    public:
        struct Point {
            int x;
            int y;
            Point *next;
        };
        enum {UP = 'w', DOWN = 's', LEFT = 'a', RIGHT = 'd'};
        Snake(Wall &wall, Food &food);
        void init_snake();
        void add_point(int x, int y);
        void delete_point();
        void destroy_point();
        bool move(char key);
        Wall &wall;
        Food &food;
        Point *pHead;
};

#endif