#ifndef _FOOD_HEAD_ 
#define _FOOD_HEAD_
#include "wall.h"
#include <stdlib.h>
class Food {
    public:
        Food(Wall &wall);
        void set_food();
        int food_x;
        int food_y;
        Wall &wall;
};

#endif