#ifndef _WALL_HEAD_
#define _WALL_HEAD_

#include<iostream>
using namespace std;

class Wall {
    public:
        enum {ROW = 26, COL = 36};
        void init_wall();
        void draw_wall();
        void set_array(int x, int y, char c);
        char get_array(int x, int y);
    private:
        char gameArray[ROW][COL];
};

#endif