#ifndef _LIB_HEAD_
#define _LIB_HEAD_
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

int getch(void);
int kbhit();

#endif