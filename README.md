# Snake 

使用`C++`编写的贪吃蛇游戏。游戏界面分为两部分，左侧是蛇的可行区域包括墙壁，蛇以及食物，右侧为游戏信息以及操作方式。使用`*`表示墙壁，`=`和`@`分别表示蛇身和蛇头，`#`表示食物，可移动区域使用空格表示。

```
* * * * * * * * * *
*                 *
*   #             * create by Shadaileng
*                 * w:   ↑
*                 * a: ←  → :d
*         = = @   * s:   ↓
*                 * 
*                 * 
*                 * 
* * * * * * * * * *
```

游戏开始时时静止的，蛇头向右，只有向上，下或右才能激活游戏。`w`,`s`,`a`,`d`四个按键分别表示上下左右，蛇不能`180°`转弯，只能执行或者`90°`转弯。

当蛇吃掉一个食物，蛇身会增加一个身段，食物会在可行区域随机设置位置

当蛇撞到墙壁或者蛇头碰到蛇身就会结束游戏。

## 游戏移动

游戏开始之后，蛇一直向前运动的，只有方向键才会转向，蛇头撞到墙壁或者蛇身游戏结束。

蛇正常移动时分为未吃到食物和吃到食物两种状态。
未吃到食物时更新蛇头位置，设为位置视为空格；
吃到食物时，蛇头位置置为食物的位置，蛇尾不变，食物重新设置位置，食物的位置不能是墙或蛇的位置。

## 代码实现

程序的入口为`main.cpp`,接收用户输入以及其他模块的调度。`wall.cpp`为墙的模块，地图以及游戏信息，是游戏的显示部分，内部维护一个二维数组，对外开放二维数组的`get`和`set`接口。`snake.cpp`负责蛇的一切行为，`food.cpp`是食物的模块。

### 墙的实现

使用分文件编写的方式，分为`wall.h`和`wall.cpp`两个文件。头文件中主要定义`Wall`类，内部开放接口`init_wall`初始化界面，`draw_wall`绘制界面，`set_array`和`get_array`分别设置和获取指定位置图形。属性`gameArray`是一个二维数组，存储地图每个位置的字符，使用一个枚举类型限定其范围。

```c
#ifndef _WALL_HEAD_
#define _WALL_HEAD_

#include<iostream>
using namespace std;

class Wall {
    public:
        enum {ROW = 26, COL = 36};
        void init_wall();
        void draw_wall();
        void set_array(int x, int y, char c);
        char get_array(int x, int y);
    private:
        char gameArray[ROW][COL];
};

#endif
```

`wall.cpp`是头文件的实现,`init_wall`初始化墙壁和可移动区域，`draw_wall`绘制`gameArray`的字符以及右侧的游戏信息

```c
#include "wall.h"

void Wall::init_wall() {
    for (int i = 0; i < ROW; ++i) {
        for (int j = 0; j < COL; ++j) {
            if (i == 0 || j == 0 || i == ROW - 1 || j == COL - 1) {
                gameArray[i][j] = '*';
            } else {
                gameArray[i][j] = ' ';
            }
        }
    }
}
void Wall::draw_wall() {
    for (int i = 0; i < ROW; ++i) {
        for (int j = 0; j < COL; ++j) {
            cout<<gameArray[i][j]<<" ";
        }
        if (i == 5) cout<<"create by Shadaileng";
        if (i == 6) cout<<"w:   ↑";
        if (i == 7) cout<<"a: ←  → :d";
        if (i == 8) cout<<"s:   ↓";
        cout<<endl;
    }
}

void Wall::set_array(int x, int y, char c) {
    // 第y行，第x列
    gameArray[y][x] = c;
}
char Wall::get_array(int x, int y) {
    // 第y行，第x列
    return gameArray[y][x];
}
```

### 蛇的实现

`snake.h`定义了`Snake`类,蛇的头部和身体的位置使用结构体`Point`表示，多个`Point`通过指针域`Point *next`串联成为蛇的整体.`Snake`内部维护了一个`Wall`对象的引用，用于蛇在运动过程中对`wall`的修改。`init_snake`方法是蛇的初始化，`add_point`方法是设置蛇头位置的操作,`delete_point`方法是未吃到食物，删除尾节点，`destroy_point`方法是销毁节点的操作。

```c
#ifndef _SNAKE_HEAD_
#define _SNAKE_HEAD_
#include<iostream>
using namespace std;
#include "wall.h"

class Snake {
    public:
        struct Point {
            int x;
            int y;
            Point *next;
        };
        Snake(Wall &wall);
        void init_snake();
        void add_point(int x, int y);
        void delete_point();
        void destroy_point();
        Wall &wall;
        Point *pHead;
};

#endif
```

`snake.cpp`是`snake.h`的实现。构造函数`Snake`初始化了成员属性,`wall`是外部地图的引用。`add_point`方法是添加蛇头节点，如果蛇头不为空，当前蛇头位置置为蛇身(`=`)，新节点置为蛇头(`@`),新旧节点之间使用指针域串联。`delete_point`是蛇移动未吃到食物时，删除蛇尾节点。`destroy_point`方法从`pHead`开始遍历，回收节点。`init_snake`方法调用`add_point`方法初始化链表。

```c
#include "snake.h" 

Snake::Snake(Wall &wall):wall(wall), pHead(NULL) {
    // 成员属性需要手动初始化
    // pHead = NULL;
}
void Snake::init_snake() {
    add_point(5, 5);
    add_point(6, 5);
    add_point(7, 5);
}
void Snake::add_point(int x, int y) {
    // 新节点
    Point *nPoint = new Point;
    nPoint->x = x;
    nPoint->y = y;
    nPoint->next = NULL;
    
    // 蛇头变为蛇身
    if (pHead != NULL) {
        wall.set_array(pHead->x, pHead->y, '=');
    }
    
    nPoint->next = pHead;
    pHead = nPoint;
    // 新节点为蛇头
    wall.set_array(pHead->x, pHead->y, '@');
}
void delete_point() {
    // 删除最后一个节点
    // 没有或只有头节点，不做操作
    if (pHead == NULL || pHead->next == NULL) return;
    Point *p_cur = pHead->next;
    Point *p_pre = pHead;
    while (p_cur->next != NULL) {
        p_pre = p_cur;
        p_cur = p_cur->next;
    }
    wall.set_array(p_cur->x, p_cur->y, ' ');
    delete p_cur;
    p_pre->next = NULL;
}
void Snake::destroy_point() {
    while(pHead != NULL) {
        Point *cur = pHead->next;
        delete pHead;
        pHead = cur;
    }
}
```

### 食物的实现

`food.h`定义了`Food`类，成员属性`wall`维护地图的引用，`food_x`和`food_y`共同表示食物的位置。`set_food`设置食物位置。

```c
#ifndef _FOOD_HEAD_ 
#define _FOOD_HEAD_
#include "wall.h"

class Food {
    public:
        Food(Wall &wall);
        void set_food();
        int food_x;
        int food_y;
        Wall &wall;
};

#endif
```

`food.cpp`是`food.h`的实现，`set_food`随机设置食物位置，死循环中判断食物位置在可移动区域。

```c
#include "food.h" 

Food::Food(Wall &wall):wall(wall) {}
void Food::set_food() {
    while(true) {
        food_x = rand() % (Wall::COL - 2) + 1;
        food_y = rand() % (Wall::ROW - 2) + 1;
        if (wall.get_array(food_x, food_y) == ' ') {
            wall.set_array(food_x, food_y, '#');
            break;
        }    
    }
}
```

### 主程序

`main.cpp`是程序的入口，主控各个模块。

```c
#include<iostream>
#include "wall.h"
#include "snake.h"
#include "food.h"
#include<ctime>
using namespace std;


int main(int argc, char *argv[]) {
    // 随机种子，保证食物每次的位置随机
    srand((unsigned int)time(NULL));
    Wall wall;
    wall.init_wall();
    Snake snake(wall);
    snake.init_snake();
    Food food(wall);
    food.set_food();
    // 绘制墙
    wall.draw_wall();
    return 0;
}
```

### 移动

`move`方法根据指令`key`判断移动方向，并修改蛇头下一步的坐标，然后判断下一坐标是否为可移动区域(空格或`#`).如果撞墙或蛇身，就结束游戏，空格就设置蛇头位置和删掉尾节点，吃到食物就只设置蛇头。

`snake.h`添加枚举类型指示方向，吃到食物的时候需要重新设置食物位置，所以`Snake`类需要添加一个`Food`对象的引用作为成员属性。

```c
#ifndef _SNAKE_HEAD_
#define _SNAKE_HEAD_
#include<iostream>
using namespace std;
#include "wall.h"
#include "food.h"

class Snake {
    public:
        struct Point {
            int x;
            int y;
            Point *next;
        };
        enum {UP = 'w', DOWN = 's', LEFT = 'a', RIGHT = 'd'};
        Snake(Wall &wall, Food &food);
        void init_snake();
        void add_point(int x, int y);
        void delete_point();
        void destroy_point();
        bool move(char key);
        Wall &wall;
        Food &food;
        Point *pHead;
};

#endif
```

`snake.cpp`实现`move`方法和构造函数设置`food`属性。

```c
Snake::Snake(Wall &wall, Food &food):wall(wall), food(food), pHead(NULL) {}
bool Snake::move(char key) {
    int x = pHead->x;
    int y = pHead->y;
    switch(key) {
        case UP:
            --y;
            break;
        case DOWN:
            ++y;
            break;
        case LEFT:
            --x;
            break;
        case RIGHT:
            ++x;
            break;
        default:
            break;
    }
    char c = wall.get_array(x, y);
    if (c == '*' || c == '=') {
        cout<<"Game Over"<<endl;
        return false;
    }
    if (c == '#') {
        food.set_food();
    } else {
        delete_point();
    }
    add_point(x, y);
    return true;
}
```

`main.cpp`测试一下移动方法

```c
#include<iostream>
#include "wall.h"
#include "snake.h"
#include "food.h"
#include<ctime>
using namespace std;


int main(int argc, char *argv[]) {
    srand((unsigned int)time(NULL));
    Wall wall;
    wall.init_wall();
    Food food(wall);
    food.set_food();
    Snake snake(wall, food);
    snake.init_snake();

    snake.move('w');
    snake.move('w');
    snake.move('a');
    
    wall.draw_wall();
    return 0;
}
```

持续接收键盘输入并移动。`vs`编译器可以导入`conio.h`头文件，调用`getch()`方法和`kbhit()`，接收输入不回显；`gnu`编译器可以在`lib_.cpp`中重写该方法。

```c
#include<iostream>
#include "wall.h"
#include "snake.h"
#include "food.h"
#include "lib_.h"
#include<ctime>
// windows
// #include<conio.h>
// #include<Windows.h>


int main(int argc, char *argv[]) {
    bool is_live = true;
    srand((unsigned int)time(NULL));
    Wall wall;
    wall.init_wall();
    Food food(wall);
    food.set_food();
    Snake snake(wall, food);
    snake.init_snake();

    system("clear");
    wall.draw_wall();
    while(is_live) {
        // 接收键盘输入，不回显
        char key = getch();
        // 不阻塞等待键盘输入，有输入返回true
        while(!kbhit()) {
            if (snake.move(key)) {
                // g++
                system("clear");
                usleep(300000);
                // vs
                // system("cls");
                // Sleep(300);
                wall.draw_wall();
            } else {
                is_live = false;
                break;
            }
        }
    }
    // wall.draw_wall();
    return 0;
}
```

## 使用make编译工程

文件结构

```
.
├── makefile
├── README.md
├── include
│   ├── food.h
│   ├── lib_.h
│   ├── main.h
│   ├── snake.h
│   └── wall.h
└── src
    ├── food.cpp
    ├── lib_.cpp
    ├── main.cpp
    ├── snake.cpp
    └── wall.cpp
```

`makefile`

```makefile
.SUFFIXES:.cpp .o

PP=g++
CC=gcc
INCLUDES=-Iinclude
LINKS=-L.
SRCSpp=src/wall.cpp \
	   src/food.cpp \
	   src/snake.cpp \
	   src/lib_.cpp \
       src/main.cpp

OBJS=$(SRCSpp:.cpp=.o) $(SRCScc:.c=.o)

EXE=main

ifeq ($(OS),Windows_NT) 
    detected_OS := Windows
else
    detected_OS := $(shell uname -o 2>/dev/null || echo Unknown)
endif

CLEAN_COMMAND := $(shell rm -f $(OBJS) || del /s /q .\*.o)

start:$(OBJS)
	$(PP) $(LINKS) -o $(EXE) $(OBJS) $(LIBS)
	@make clean
	@echo "Compelete!!"
%.o:%.cpp
	$(PP) -o $@ -c $< $(INCLUDES)
%.o:%.c
	$(CC) -o $@ -c $< $(INCLUDES)
clean:
	$(CLEAN_COMMAND)
test:
	@echo "detected_OS is " $(detected_OS)
	@echo "LIBS is " $(LIBS)
	@echo "CLEAN_COMMAND is " $(CLEAN_COMMAND)
```


# 备注

`scp`传输文件: scp -r !(main*) user@host:/path/to/snake_by_cpp