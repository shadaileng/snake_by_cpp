#include "main.h"

#if TEST

int main(int argc, char *argv[]) {
    bool is_live = true;
    srand((unsigned int)time(NULL));
    Wall wall;
    wall.init_wall();
    Food food(wall);
    food.set_food();
    Snake snake(wall, food);
    snake.init_snake();

    system("clear");
    wall.draw_wall();
    while(is_live) {
        char key = getch();
        while(!kbhit()) {
            if (snake.move(key)) {
                // system("clear");
                wall.draw_wall();
                usleep(300000);
            } else {
                is_live = false;
                break;
            }
        }
    }
    
    // wall.draw_wall();
    return 0;
}

#else

int main(int argc, char *argv[]) {
    // while(!kbhit()) puts("Press a key!");
    // printf("You pressed '%c'!/n", getchar());
    // printf("\033[40;37mhello world\033[3;4H");
    system("clear");
    cout<<"\033["<<3<<";"<<4<<"H"<<"Hello";
    int a;
    cin>>a;
    return 0;
}
#endif
