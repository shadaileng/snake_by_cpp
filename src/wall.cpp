#include "wall.h"

void Wall::init_wall() {
    for (int i = 0; i < ROW; ++i) {
        for (int j = 0; j < COL; ++j) {
            if (i == 0 || j == 0 || i == ROW - 1 || j == COL - 1) {
                gameArray[i][j] = '*';
            } else {
                gameArray[i][j] = ' ';
            }
        }
    }
}
void Wall::draw_wall() {
    for (int i = 0; i < ROW; ++i) {
        for (int j = 0; j < COL; ++j) {
            cout<<"\033["<<i<<";"<<(j * 2)<<"H";
            cout<<gameArray[i][j]<<" ";
        }
        if (i == 5) cout<<"create by Shadaileng";
        if (i == 6) cout<<"w:   ↑";
        if (i == 7) cout<<"a: ←  → :d";
        if (i == 8) cout<<"s:   ↓";
        cout<<endl;
    }
}

void Wall::set_array(int x, int y, char c) {
    // 第y行，第x列
    gameArray[y][x] = c;
}
char Wall::get_array(int x, int y) {
    // 第y行，第x列
    return gameArray[y][x];
}