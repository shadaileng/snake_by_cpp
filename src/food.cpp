#include "food.h" 

Food::Food(Wall &wall):wall(wall) {}
void Food::set_food() {
    while(true) {
        food_x = rand() % (Wall::COL - 2) + 1;
        food_y = rand() % (Wall::ROW - 2) + 1;
        if (wall.get_array(food_x, food_y) == ' ') {
            wall.set_array(food_x, food_y, '#');
            break;
        }    
    }
}