.SUFFIXES:.cpp .o

PP=g++
CC=gcc
INCLUDES=-Iinclude
LINKS=-L.
SRCSpp=src/wall.cpp \
	   src/food.cpp \
	   src/snake.cpp \
	   src/lib_.cpp \
       src/main.cpp

OBJS=$(SRCSpp:.cpp=.o) $(SRCScc:.c=.o)

EXE=main

ifeq ($(OS),Windows_NT) 
    detected_OS := Windows
else
    detected_OS := $(shell uname -o 2>/dev/null || echo Unknown)
endif

CLEAN_COMMAND := $(shell rm -f $(OBJS) || del /s /q .\*.o)

start:$(OBJS)
	$(PP) $(LINKS) -o $(EXE) $(OBJS) $(LIBS)
	@make clean
	@echo "Compelete!!"
%.o:%.cpp
	$(PP) -o $@ -c $< $(INCLUDES)
%.o:%.c
	$(CC) -o $@ -c $< $(INCLUDES)
clean:
	$(CLEAN_COMMAND)
test:
	@echo "detected_OS is " $(detected_OS)
	@echo "LIBS is " $(LIBS)
	@echo "CLEAN_COMMAND is " $(CLEAN_COMMAND)